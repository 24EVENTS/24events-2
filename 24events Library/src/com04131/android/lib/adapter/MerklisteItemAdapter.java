package com04131.android.lib.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import com04131.android.lib.MainActionBarActivity;
import com04131.android.lib.R;
import com04131.android.lib.data.MerklisteItem;

import java.util.List;

/**
 * User: Goddchen
 * Date: 16.04.13
 */
public class MerklisteItemAdapter extends ArrayAdapter<MerklisteItem> {

    public MerklisteItemAdapter(Context context, List<MerklisteItem> objects) {
        super(context, android.R.layout.simple_list_item_multiple_choice, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem_merkliste, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.text)).setText(getItem(position).title);
        convertView.findViewById(R.id.checkbox).setVisibility(((MainActionBarActivity) getContext()).actionMode ==
                null ? View.GONE : View.VISIBLE);
        ((CheckBox) convertView.findViewById(R.id.checkbox)).setChecked(((ListView) parent).isItemChecked(position));
        return convertView;
    }
}
