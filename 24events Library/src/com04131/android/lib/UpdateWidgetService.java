package com04131.android.lib;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.List;

public class UpdateWidgetService extends IntentService {

    public UpdateWidgetService() {
        super(UpdateWidgetService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent arg0) {
        try {
            AppWidgetManager appWidgetManager = AppWidgetManager
                    .getInstance(this);
            RemoteViews views = new RemoteViews(getPackageName(),
                    R.layout.widget);
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(this);
            prefs.edit().remove(AppWidgetProvider.PREF_DATA)
                    .remove(AppWidgetProvider.PREF_POSITION).commit();
            views.setViewVisibility(R.id.loading, View.VISIBLE);
            views.setViewVisibility(R.id.content, View.GONE);
            views.setViewVisibility(R.id.refresh_wrapper, View.GONE);
            appWidgetManager.updateAppWidget(new ComponentName(this,
                    AppWidgetProvider.class), views);
            try {
                /*List<Event> events = new ArrayList<Event>();
                InputStream in = new URL(getString(R.string.url_events))
						.openStream();
				byte[] buffer = new byte[1024];
				StringBuilder builder = new StringBuilder(8192);
				int count = 0;
				while ((count = in.read(buffer)) != -1) {
					builder.append(new String(buffer, 0, count));
				}
				in.close();
				JSONArray jsonEvents = new JSONArray(builder.toString());*/
                List<Event> events = Helper.getEvents(this);
                Log.d(getPackageName(), "Found " + events.size() + " events");
                SimpleDateFormat dateSdf = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat timeSdf = new SimpleDateFormat("HH:mm:ss");
//                for (int i = 0; i < jsonEvents.length(); i++) {
                for (Event event : events) {
                    /*Event event = new Event();
                    event.title = jsonEvents.getJSONObject(i)
                            .optString("title");
                    event.venue = jsonEvents.getJSONObject(i).optString(
                            "venue");*/
                    try {
                        event.startDate = dateSdf.parse(TextUtils.isEmpty(event.dates) ? "1970-01-01" : event.dates);
                                /*jsonEvents
                                .getJSONObject(i).optString("dates",
                                        "1970-01-01"));*/
                    } catch (Exception e) {
                        Log.w(getClass().getSimpleName(), "Error parsing date",
                                e);
                        event.startDate = dateSdf.parse("1970-01-01");
                    }
                    try {
                        event.startTime = timeSdf.parse(TextUtils.isEmpty(event.times) ? "00:00:00" : event.times);
                                /*jsonEvents
                                .getJSONObject(i)
                                .optString("times", "00:00:00"));*/
                    } catch (Exception e) {
                        Log.w(getClass().getSimpleName(), "Error parsing time",
                                e);
                        event.startTime = timeSdf.parse("00:00:00");
                    }
                    /*event.id = jsonEvents.getJSONObject(i).optInt("id");
                    events.add(event);*/
                }
                Helper.storeEvents(this, events);
                prefs.edit().putInt(AppWidgetProvider.PREF_POSITION, 0)
                        .commit();
                // updateEvents(events, appWidgetManager);
//                Helper.updateWidget(this, false);
            } catch (Exception e) {
                Log.e(getPackageName(), "Error getting events", e);
                // updateEvents(null, appWidgetManager);
            } finally {
                Helper.updateWidget(this, false);
            }
            // Event event = new Event();
            // event.description = "Desc";
            // event.locatation = "Disco";
            // event.startDate = new Date();
            // event.startTime = new Date();
            // events.add(event);
            // event = new Event();
            // event.description = "Desc2";
            // event.locatation = "Club";
            // event.startDate = new Date();
            // event.startTime = new Date();
            // events.add(event);
        } catch (Exception e) {
            Log.e(getPackageName(), "Error updating widget", e);
        }
    }

    // private void updateEvents(List<Event> events,
    // AppWidgetManager appWidgetManager) {
    // AppWidgetProvider.events = events;
    // AppWidgetProvider.currentPosition = 0;
    // appWidgetManager.updateAppWidget(new ComponentName(this,
    // AppWidgetProvider.class), AppWidgetProvider.buildRemoteViews(
    // this, false));
    // }
}
