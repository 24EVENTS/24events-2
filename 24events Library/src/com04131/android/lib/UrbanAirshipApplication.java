package com04131.android.lib;

import android.app.Application;
import android.util.Log;

import com.codeslap.persistence.PersistenceConfig;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;

import com04131.android.lib.data.MerklisteItem;

public class UrbanAirshipApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        initUrbanAirship();
        PersistenceConfig.registerSpec(2).match(MerklisteItem.class);
    }

    private void initUrbanAirship() {
        try {
            UAirship.takeOff(this);
            PushManager.enablePush();
            Log.d(getPackageName(), "UrbanAirship APID: "
                    + PushManager.shared().getPreferences().getPushId());
            PushManager.shared()
                    .setIntentReceiver(UrbanAirshipIntentReceiver.class);
        } catch (Exception e) {
            Log.e(UrbanAirshipApplication.class.getSimpleName(), "Error initializing UA", e);
        }
    }
}
