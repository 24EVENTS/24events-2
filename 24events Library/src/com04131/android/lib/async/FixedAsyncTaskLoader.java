package com04131.android.lib.async;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * User: Goddchen
 * Date: 16.04.13
 */
public abstract class FixedAsyncTaskLoader<T> extends AsyncTaskLoader<T> {

    private T mData;

    public FixedAsyncTaskLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        if (mData != null) {
            deliverResult(mData);
        } else {
            forceLoad();
        }
    }

    @Override
    protected void onReset() {
        super.onReset();
        mData = null;
    }
}
