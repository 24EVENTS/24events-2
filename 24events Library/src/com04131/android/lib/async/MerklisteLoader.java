package com04131.android.lib.async;

import android.content.Context;
import android.util.Log;
import com.codeslap.persistence.Persistence;
import com04131.android.lib.data.MerklisteItem;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * User: Goddchen
 * Date: 16.04.13
 */
public class MerklisteLoader extends FixedAsyncTaskLoader<List<MerklisteItem>> {

    public MerklisteLoader(Context context) {
        super(context);
    }

    @Override
    public List<MerklisteItem> loadInBackground() {
        try {
            List<MerklisteItem> items = Persistence.getAdapter(getContext()).findAll(MerklisteItem.class);
            Collections.sort(items, new Comparator<MerklisteItem>() {
                @Override
                public int compare(MerklisteItem lhs, MerklisteItem rhs) {
                    return -(Long.valueOf(lhs.created).compareTo(rhs.created));
                }
            });
            return items;
        } catch (Exception e) {
            Log.e(MerklisteLoader.class.getSimpleName(), "Error loading Merkliste", e);
            return null;
        }
    }
}
